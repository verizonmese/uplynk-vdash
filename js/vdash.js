// var primary_enabled = false;
// var secondary_enabled = false;

$(function(){

    if ($(window).width() < 300){
        console.log($(window).width());
        $("h2").on("click", function(){
            var currentDiv = $(this).next("div");
            if (currentDiv.is(":visible")){
                currentDiv.hide();
            }else{
                currentDiv.show();
            }
        }); 
    }
       
    var slicers = [];

    slicer = {};
    slicer.ip = $('#primary_slicer_ip').val();
    slicer.port = $('#primary_slicer_port').val();
    slicer.type = 'primary';
    slicers.push(slicer);
    slicer = {};
    slicer.ip = $('#secondary_slicer_ip').val();
    slicer.port = $('#secondary_slicer_port').val();
    slicer.type = 'secondary';
    slicers.push(slicer);

//    var slicer_ip = $('#primary_slicer_ip').val();
//    var slicer_port = $('#primary_slicer_port').val();
//    var base_url = "http://" + slicer_ip + ":" + slicer_port;
//    var contact_attemps = 0;
    
    // util
    var formatTime = function(value){
        if(!value) return "00.00";
        console.log("value: " + value);
        return value.toFixed(2);
    }

    // model
    var slicer_model = {
        err: ko.observable(0),
        remaining: ko.observable(''),
        state: ko.observable(0),
        state_name: ko.observable('No slicer enabled')
    };

    var poll = function(){
        console.log('poll slicers.len: ' + slicers.length);
	    var poll_timer = setInterval(function(){
        console.log('polling');
        if(slicers.length < 1){
            clearInterval(poll_timer);
            slicer_model.state_name('No slicer enabled');   
            return; //nothing to do
        }
        for(i=0;i <  slicers.length; i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            call_url = base_url + '/state?callback=?';
            //console.log('call_url: ' + call_url);
            $.ajax({
                url: call_url,
                dataType: 'json',
                crossDomain: true,
                timeout:500
            })
            .success(function(data){
                console.log(data);
                slicer_model.err(data.error);
                slicer_model.remaining(formatTime(data.remaining));
                slicer_model.state(data.state);
                slicer_model.state_name(data.state_name);
                switch(data.state_name){
                    case "Capture":
                        in_capture();
                        break;
                    case "Ad":
                        if(data.remaining > 0 ){
                            in_ad_with_duration();
                        }
                        else{
                            in_ad();
                        }
                        break;

                    case "Blackout":
                        in_blackout();
                        break;
                    case "Replace":
                        in_replace();
                        break;
                    default:
                        console.log('unknown state: ' + data.state);
                        break;
                };
            })
            .error(function() {
                console.log('error: request timedout.  Check network connection.');
                slicer_model.state_name('Error: Check network connection');
                in_error();
            });
        }
        /*
        $.getJSON(base_url+'/state?callback=?')
			.done(function(data){
				console.log(data);
				slicer_model.err(data.error);
			    slicer_model.remaining(formatTime(data.remaining));
				slicer_model.state(data.state);
                slicer_model.state_name(data.state_name);
                switch(data.state_name){
                    case "Capture":
                        in_capture();
                        break;
                    case "Ad":
                        if(data.remaining > 0 ){
                            in_ad_with_duration();
                        }
                        else{
                            in_ad();
                        }
                        break;
                    default:
                        console.log('unknown state: ' + data.state);
                        break;
                };
            })
            .fail(function(){
                console.log('error... probably cannot reach server');
                slicer_model.state_name('Error: Most likely cannot reach server.');
            });
         */
        },1000);
	};

	ko.applyBindings(slicer_model);
    //poll();

    // checkbox toggled handlers
    $('#primary_enable').change(function(){
        console.log("this = ", this.checked);
        if(this.checked){
            // should validate this is a good ip and there is a slicer there.
            slicer = {};
            slicer.ip = $('#primary_slicer_ip').val();
            slicer.port = $('#primary_slicer_port').val();
            slicer.type = 'primary';
            slicers.push(slicer);
        } else {
            for(i=0;i< slicers.length;i++){
                s = slicers[i];
                if (s.type == 'primary'){
                    slicers.splice(i,1);
                }
            }
        }
        poll();
    });
    
    $('#secondary_enable').change(function(){
        if(this.checked){
            // should validate this is a good ip and there is a slicer there.
            slicer = {};
            slicer.ip = $('#secondary_slicer_ip').val();
            slicer.port = $('#secondary_slicer_port').val();
            slicer.type = 'secondary';
            slicers.push(slicer);
        } else {
            for(i=0;i< slicers.length;i++){
                s = slicers[i];
                if (s.type == 'secondary'){
                    slicers.splice(i,1);
                }
            }
        }
        poll();
    });
	// button click handlers

	// Ad Break Start
	$('#btn_start_break').click(function() {
        console.log('ad_break_start called');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/pod_start?callback=?')
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    });

	// Ad Break Stop
    $('#btn_end_break').click(function() {
        end_break();
    });

	// Ad Break with duration
    $('#btn_insert_break_with_duration').click(function() {
        console.log('ad_break_with_duration called');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/replace_pod?callback=?&duration=' + $('#txt_ad_break_duration').val())
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    });

	// +30 to break duration
    $('#btn_add_30_seconds').click(function() {
        console.log('ad_break_add_30');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/replace_pod?callback=?&duration=30')
                .done(function(data){
                    console.log(data);
                    poll();
                });
            }
    });

    
	// -30 to break duration (alpha)
    $('#btn_subtract_30_seconds').click(function() {
        console.log('ad_break_subtract_30');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/replace_pod?callback=?&duration=-30')
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    });
	
    // Ad Break Stop
    $('#btn_end_break2').click(function() {
        end_break();
    });

    // Blackout Start
    $('#btn_start_blackout').click(function(){
        console.log('blackout start');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/blackout?callback=?')
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    });

    // Blackout Stop
    $('#btn_end_blackout').click(function(){
        console.log('blackout end');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/content_start?callback=?')
                .done(function(data){
                    console.log(data);
                    poll();
                });
            }
    });

    // replace stream with VOD content
    $('#btn_replace_content').click(function() {
        console.log('replace_content called');
        rep_array = [];

        replace_obj = {};
        replace_obj['external_id'] = $('#txt_replacement_content_ext_id').val();
        replace_obj['duration'] = dur = parseInt($('#txt_replacement_content_duration').val());

        rep_array.push(replace_obj);

        rep_obj = encodeURIComponent(JSON.stringify(rep_array));
        rc_url = base_url+'/replace_content?callback=?&duration=' + dur + '&replacements='+rep_obj;
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/replace_content?callback=?&duration=' + dur + '&replacements='+rep_obj)
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    });

    // end replace content break
    $('#btn_end_replace').click(function() {
        console.log('end_replace called');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/content_start?callback=?')
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    });

    // End break functionality called from multiple btns
    var end_break = function(){
        console.log('ad_break_stop called');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            $.getJSON(base_url+'/pod_end?callback=?')
                .done(function(data){
                    console.log(data);
                    poll();
                });
        }
    }

    // Content Start (optional blackout id)
    $('#btn_start_conditional').click(function() {
        console.log('start_conditional called');
        for(i=0;i<slicers.length;i++){
            base_url = 'http://' + slicers[i].ip + ':' + slicers[i].port;
            
            var _title;
            if($('#txt_blackout_title').val() ){
               
                var temp = $('#txt_blackout_title').val();
                //meta_obj = encodeURIComponent(JSON.stringify(o));
                meta_obj = encodeURIComponent(temp);
                _title = "&title="+meta_obj;
                
            }

            $.getJSON(base_url + '/content_start?callback=?' + _title)
                .done(function(data){
                    console.log("content_start(data): " +JSON.stringify(data));
            });
            
            if($('#txt_blackout_id').val() ){
                o = {};
                o['blackout_id'] = $('#txt_blackout_id').val();
                meta_obj = encodeURIComponent(JSON.stringify(o));
                $.getJSON(base_url+'/add_meta?callback=?&meta=' + meta_obj)
                    .done(function(data){
                        console.log("add_meta(data): " +JSON.stringify(data));
                    });
            }
            
            poll();
        }
    });

    
    // Button States
    var in_capture = function(){
        //console.log('called in_capture');
        $('body').css('background-color','white');
        $('#btn_start_break').removeAttr('disabled');
        $('#btn_end_break').attr('disabled','disabled');
    
        // If returning from ad_with_duration
        $('#RemainContain').css("visibility","hidden");
        $('#btn_insert_break_with_duration').removeAttr('disabled');
        $('#btn_add_30_seconds').attr('disabled','disabled');
        $('#btn_subtract_30_seconds').attr('disabled','disabled');
        $('#btn_end_break2').attr('disabled','disabled');

        // If returning from blackout
        $('#btn_start_blackout').removeAttr('disabled');
        //$('#btn_end_blackout').attr('disabled','disabled');

        // If returning from replace content
        $('#btn_end_replace').attr('disabled','disabled');
        $('#btn_replace_content').removeAttr('disabled');
    }

    var in_ad_with_duration = function(){
        console.log('called in_ad_with_duration');
        $('body').css('background-color','#CCFFCC');
        $('#RemainContain').css("visibility","visible");
        $('#btn_insert_break_with_duration').attr('disabled','disabled');
        $('#btn_add_30_seconds').removeAttr('disabled');
        $('#btn_subtract_30_seconds').removeAttr('disabled');
        $('#btn_end_break2').removeAttr('disabled');
    }
    
    var in_ad = function(){
        console.log('called in_ad');
        $('body').css('background-color','#CCFFCC');
        $('#btn_start_break').attr('disabled','disabled');
        $('#btn_end_break').removeAttr('disabled');
    }

    var in_blackout = function(){
        console.log('called in_blackout');
        $('body').css('background-color','#666666');
        $('#btn_start_blackout').attr('disabled','disabled');
        $('#btn_end_blackout').removeAttr('disabled');
    }

    var in_replace = function(){
        console.log('called in_replace');
        $('body').css('background-color', '#DDEEFF');
        $('#RemainContain').css("visibility","visible");
        $('#btn_end_replace').removeAttr('disabled');
        $('#btn_replace_content').attr('disabled','disabled');
    }

    var in_error = function(){
        console.log('called in_error');
        $('body').css('background-color','#FFCC99');
    }

});