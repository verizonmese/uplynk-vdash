INSTALL

SUMMARY

This file includes instructions for installing the upLynk ad break insertion 
dashboard.

INSTRUCTIONS

In order to use the dashboard the upLynk live slicer must have its web apis 
enabled.  To enable the webservices find your live slicer configuration
file.  It's usually located at /etc/uplynk.conf. We need to add the
configuration options api_port which requires the ip address for your slicer
and the port it runs on.  Below is an example of this entry.

api_port: 192.168.10.202:65009

After you finish editing your configuration file, save the changes. Now
restart your live slicer.

Now that the live slicer is ready to be controlled by the dashboard, we'll
install the dashboard and required software.

Install your favorite httpd server on the live slicer server hardware.  Copy 
the complete contents of this directory and its children to the document 
root of your httpd server.

From the command line, open the file index.html in your favorite text editor. 
Find the configuration section near the top of the file. Change slicer_ip 
from 192.168.10.202 to your slicer server's IP address.  Unless you've 
selected a different port, you can leave the slicer_port default at 65009.

Save and exit the editor.

Using a web browser, load the dashboard's index.html file. Current State 
should change from "Start" to "Capture" signaling the dashboard is
communicating with the live_slicer and is ready to send commands.

USAGE

Indeterminate break is a break that is manually started by presing the 
Start Ad Break button, and manually ended by pressing the End Ad Break
button.  This type of break might be used during a timeout due to injury
in a football game where the length of time needed to attend to the
injured athelete is unknown and ads are played in the meantime.

A break with duration is the more common ad break type.  The operator / 
producer supplies a duration in seconds.  The break is then inserted.
This dashboard also provides a button that adds 30 additional seconds,
a button that removes 30 seconds and an End Ad Break button for returning
from the ad break immediately.

Blackout tells the slicer to stop capturing and stand by.  It is started with 
the Start Blackout button. It is ended by clicking the End Blackout button.
This begins capture again.

VOD Replacement allows interruption of the capturing stream by an asset from
your library. The asset must have an external ID set for it.  This can be done
in the upLynk CMS editor pane after selecting your asset.  To use it as
replacement content you supply the external ID and the duration of the 
replacement content to the dashboard form and click the Replace Content button.
You can interrupt the replacment content and return to capture early by
clicking the End Replacement button.

